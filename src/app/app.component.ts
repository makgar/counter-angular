import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Counter-Angular';
  public currentCount = 0;

  public handleDecrease() {
    this.currentCount--;
  }

  public handleReset() {
    this.currentCount = 0;
  }

  public handleIncrease() {
    this.currentCount++;
  }
}
